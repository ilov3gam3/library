<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/master/head.jsp" %>
<%@page pageEncoding="UTF-8" %>
<div class="container mt-2">
  <div class="breadcrumb page-header text-center">
    <div class="container">
      <h1 class="page-title">Trang quản trị</h1>
    </div>
  </div>
  <div class="col-12 justify-content-center mt-2">
    <div class="row">
      <div class="col-6 ">
        <a href="${pageContext.request.contextPath}/admin/genre" style="width: 100%">
          <button class="btn btn-primary btn-block">
            Thể loại
          </button>
        </a>
      </div>
      <div class="col-6 ">
        <a href="${pageContext.request.contextPath}/admin/author" style="width: 100%">
          <button class="btn btn-primary btn-block">
            Tác giả
          </button>
        </a>
      </div>
    </div>
  </div>
  <div class="col-12 justify-content-center mt-2">
    <div class="row">
      <div class="col-6 ">
        <a href="${pageContext.request.contextPath}/admin/book" style="width: 100%">
          <button class="btn btn-primary btn-block">
            Sách
          </button>
        </a>
      </div>
      <div class="col-6 ">
        <a href="#" style="width: 100%">
          <button class="btn btn-primary btn-block">
            chưa có
          </button>
        </a>
      </div>
    </div>
  </div>
</div>
<%@ include file="/master/foot.jsp" %>