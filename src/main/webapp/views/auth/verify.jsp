<%@page contentType="text/html;" pageEncoding="utf-8" %>
<%@ include file="/master/head.jsp" %>
<div class="breadcrumb page-header text-center">
  <div class="container">
    <h1 class="page-title text-primary">${message_success}</h1>
    <h1 class="page-title text-danger">${message_error  }</h1>
  </div>
</div>
<%@ include file="/master/foot.jsp" %>