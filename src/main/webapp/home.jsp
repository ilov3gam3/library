<%@page contentType="text/html;" pageEncoding="utf-8" %>
<%@ include file="/master/head.jsp" %>
<style id="pagebuilder-frontend-stylesheet" type="text/css">
    #row-dhw8p8enl {
    }

    @media (max-width: 991px) {
        #row-dhw8p8enl {
        }
    }

    @media (max-width: 575px) {
        #row-dhw8p8enl {
        }
    }

    #column-zwkfnrf8s {
    }

    @media (max-width: 991px) {
        #column-zwkfnrf8s {
        }
    }

    @media (max-width: 575px) {
        #column-zwkfnrf8s {
        }
    }

    #row-i5oou8rez {
    }

    @media (max-width: 991px) {
        #row-i5oou8rez {
        }
    }

    @media (max-width: 575px) {
        #row-i5oou8rez {
        }
    }

    #column-rifrtujuf {
    }

    @media (max-width: 991px) {
        #column-rifrtujuf {
        }
    }

    @media (max-width: 575px) {
        #column-rifrtujuf {
        }
    }

    #addon-xn7737f39 .pb-spacer-inner {
        height: 20px;
    }

    @media (max-width: 991px) {
        #addon-xn7737f39 .pb-spacer-inner {
            height: 20px;
        }
    }

    @media (max-width: 575px) {
        #addon-xn7737f39 .pb-spacer-inner {
            height: 20px;
        }
    }

    #row-cbgtkl9u3 {
    }

    @media (max-width: 991px) {
        #row-cbgtkl9u3 {
        }
    }

    @media (max-width: 575px) {
        #row-cbgtkl9u3 {
        }
    }

    #column-swsitjal1 {
    }

    @media (max-width: 991px) {
        #column-swsitjal1 {
        }
    }

    @media (max-width: 575px) {
        #column-swsitjal1 {
        }
    }

    #addon-ce6c4eqdc .pb-banner-text {
        top: 0px;
        left: 40px;
        text-align: left;
    }

    #addon-ce6c4eqdc .pb-banner-subtitle {
        font-size: 14px;
        line-height: 21px;
        color: #ffffff
    }

    #addon-ce6c4eqdc .pb-banner-title {
        font-size: 40px;
        line-height: 50px;
        color: #ffffff
    }

    #addon-ce6c4eqdc .pb-banner-desc {
        font-size: 14px;
        line-height: 21px;
        color:
    }

    @media (max-width: 991px) {
        #addon-ce6c4eqdc .pb-banner-text {
            top: 20px;
            left: 20px;
        }

        #addon-ce6c4eqdc .pb-banner-subtitle {
            font-size: 14px;
            line-height: 21px;
        }

        #addon-ce6c4eqdc .pb-banner-title {
            font-size: 30px;
            line-height: 35px;
        }

        #addon-ce6c4eqdc .pb-banner-desc {
            font-size: 12px;
            line-height: 18px;
        }
    }

    @media (max-width: 575px) {
        #addon-ce6c4eqdc .pb-banner-text {
            top: 20px;
            left: 20px;
        }

        #addon-ce6c4eqdc .pb-banner-subtitle {
            font-size: 14px;
            line-height: 21px;
        }

        #addon-ce6c4eqdc .pb-banner-title {
            font-size: 30px;
            line-height: 35px;
        }

        #addon-ce6c4eqdc .pb-banner-desc {
            font-size: 10px;
            line-height: 15px;
        }
    }

    #column-agi747c3c {
    }

    @media (max-width: 991px) {
        #column-agi747c3c {
        }
    }

    @media (max-width: 575px) {
        #column-agi747c3c {
        }
    }

    #addon-9uvpqfyna .pb-banner-text {
        top: 25px;
        left: 40px;
        text-align: left;
    }

    #addon-9uvpqfyna .pb-banner-subtitle {
        font-size: 14px;
        line-height: 21px;
        color: #ffffff
    }

    #addon-9uvpqfyna .pb-banner-title {
        font-size: 24px;
        line-height: 30px;
        color: #ffffff
    }

    #addon-9uvpqfyna .pb-banner-desc {
        font-size: 14px;
        line-height: 21px;
        color:
    }

    @media (max-width: 991px) {
        #addon-9uvpqfyna .pb-banner-text {
            top: 20px;
            left: 20px;
        }

        #addon-9uvpqfyna .pb-banner-subtitle {
            font-size: 14px;
            line-height: 21px;
        }

        #addon-9uvpqfyna .pb-banner-title {
            font-size: 24px;
            line-height: 30px;
        }

        #addon-9uvpqfyna .pb-banner-desc {
            font-size: 12px;
            line-height: 18px;
        }
    }

    @media (max-width: 575px) {
        #addon-9uvpqfyna .pb-banner-text {
            top: 20px;
            left: 20px;
        }

        #addon-9uvpqfyna .pb-banner-subtitle {
            font-size: 14px;
            line-height: 21px;
        }

        #addon-9uvpqfyna .pb-banner-title {
            font-size: 24px;
            line-height: 30px;
        }

        #addon-9uvpqfyna .pb-banner-desc {
            font-size: 10px;
            line-height: 15px;
        }
    }

    #column-o4xhr90y6 {
    }

    @media (max-width: 991px) {
        #column-o4xhr90y6 {
        }
    }

    @media (max-width: 575px) {
        #column-o4xhr90y6 {
        }
    }

    #addon-77nx3txla .pb-banner-text {
        top: 0px;
        left: 30px;
        text-align: left;
    }

    #addon-77nx3txla .pb-banner-subtitle {
        font-size: 14px;
        line-height: 21px;
        color: #ffffff
    }

    #addon-77nx3txla .pb-banner-title {
        font-size: 24px;
        line-height: 30px;
        color: #ffffff
    }

    #addon-77nx3txla .pb-banner-desc {
        font-size: 14px;
        line-height: 21px;
        color:
    }

    @media (max-width: 991px) {
        #addon-77nx3txla .pb-banner-text {
            top: 20px;
            left: 20px;
        }

        #addon-77nx3txla .pb-banner-subtitle {
            font-size: 14px;
            line-height: 21px;
        }

        #addon-77nx3txla .pb-banner-title {
            font-size: 24px;
            line-height: 30px;
        }

        #addon-77nx3txla .pb-banner-desc {
            font-size: 12px;
            line-height: 18px;
        }
    }

    @media (max-width: 575px) {
        #addon-77nx3txla .pb-banner-text {
            top: 20px;
            left: 20px;
        }

        #addon-77nx3txla .pb-banner-subtitle {
            font-size: 14px;
            line-height: 21px;
        }

        #addon-77nx3txla .pb-banner-title {
            font-size: 24px;
            line-height: 30px;
        }

        #addon-77nx3txla .pb-banner-desc {
            font-size: 10px;
            line-height: 15px;
        }
    }

    #addon-kknbu0r4f .pb-spacer-inner {
        height: 20px;
    }

    @media (max-width: 991px) {
        #addon-kknbu0r4f .pb-spacer-inner {
            height: 40px;
        }
    }

    @media (max-width: 575px) {
        #addon-kknbu0r4f .pb-spacer-inner {
            height: 30px;
        }
    }

    #addon-9hybhk8fh .pb-banner-text {
        top: 0px;
        left: 30px;
        text-align: left;
    }

    #addon-9hybhk8fh .pb-banner-subtitle {
        font-size: 14px;
        line-height: 21px;
        color: #ffffff
    }

    #addon-9hybhk8fh .pb-banner-title {
        font-size: 24px;
        line-height: 30px;
        color: #ffffff
    }

    #addon-9hybhk8fh .pb-banner-desc {
        font-size: 14px;
        line-height: 21px;
        color:
    }

    @media (max-width: 991px) {
        #addon-9hybhk8fh .pb-banner-text {
            top: 20px;
            left: 20px;
        }

        #addon-9hybhk8fh .pb-banner-subtitle {
            font-size: 14px;
            line-height: 21px;
        }

        #addon-9hybhk8fh .pb-banner-title {
            font-size: 24px;
            line-height: 30px;
        }

        #addon-9hybhk8fh .pb-banner-desc {
            font-size: 12px;
            line-height: 18px;
        }
    }

    @media (max-width: 575px) {
        #addon-9hybhk8fh .pb-banner-text {
            top: 20px;
            left: 20px;
        }

        #addon-9hybhk8fh .pb-banner-subtitle {
            font-size: 14px;
            line-height: 21px;
        }

        #addon-9hybhk8fh .pb-banner-title {
            font-size: 24px;
            line-height: 30px;
        }

        #addon-9hybhk8fh .pb-banner-desc {
            font-size: 10px;
            line-height: 15px;
        }
    }

    #row-6lllqhkxu {
        padding-top: 25px;
        padding-bottom: 25px;
    }

    @media (max-width: 991px) {
        #row-6lllqhkxu {
        }
    }

    @media (max-width: 575px) {
        #row-6lllqhkxu {
        }
    }

    #column-4lfbrt40h {
    }

    @media (max-width: 991px) {
        #column-4lfbrt40h {
        }
    }

    @media (max-width: 575px) {
        #column-4lfbrt40h {
        }
    }

    #addon-5rhyxkq7p .pb-service-box {
        text-align: left;
    }

    #addon-5rhyxkq7p .pb-service-icon i {
        font-size: 34px;
    }

    #addon-5rhyxkq7p .pb-service-icon img {
        width: 25%;
    }

    #column-2y1kdp964 {
    }

    @media (max-width: 991px) {
        #column-2y1kdp964 {
        }
    }

    @media (max-width: 575px) {
        #column-2y1kdp964 {
        }
    }

    #addon-rrcffktlp .pb-service-box {
        text-align: left;
    }

    #addon-rrcffktlp .pb-service-icon i {
        font-size: 34px;
    }

    #addon-rrcffktlp .pb-service-icon img {
        width: 25%;
    }

    #column-rma1buzfv {
    }

    @media (max-width: 991px) {
        #column-rma1buzfv {
        }
    }

    @media (max-width: 575px) {
        #column-rma1buzfv {
        }
    }

    #addon-i5j08rk6d .pb-service-box {
        text-align: left;
    }

    #addon-i5j08rk6d .pb-service-icon i {
        font-size: 34px;
    }

    #addon-i5j08rk6d .pb-service-icon img {
        width: 25%;
    }

    #column-acjkqul12 {
    }

    @media (max-width: 991px) {
        #column-acjkqul12 {
        }
    }

    @media (max-width: 575px) {
        #column-acjkqul12 {
        }
    }

    #addon-381rl0da0 .pb-service-box {
        text-align: left;
    }

    #addon-381rl0da0 .pb-service-icon i {
        font-size: 34px;
    }

    #addon-381rl0da0 .pb-service-icon img {
        width: 25%;
    }

    #row-dlwmhtm0z {
        background-color: #f8f8f8;
        padding-top: 51px;
        padding-bottom: 44px;
    }

    @media (max-width: 991px) {
        #row-dlwmhtm0z {
        }
    }

    @media (max-width: 575px) {
        #row-dlwmhtm0z {
        }
    }

    #column-i8iorhy4b {
    }

    @media (max-width: 991px) {
        #column-i8iorhy4b {
        }
    }

    @media (max-width: 575px) {
        #column-i8iorhy4b {
        }
    }

    #addon-eeieeg3ez .pb-heading {
        color: ;
        text-align: center;
        font-size: 24px;
    }

    @media (max-width: 991px) {
        #addon-eeieeg3ez .pb-heading {
            font-size: 22px;
        }
    }

    @media (max-width: 575px) {
        #addon-eeieeg3ez .pb-heading {
            font-size: 22px;
        }
    }

    #column-79i6ye0wq {
    }

    @media (max-width: 991px) {
        #column-79i6ye0wq {
        }
    }

    @media (max-width: 575px) {
        #column-79i6ye0wq {
        }
    }

    #column-qpb72cal9 {
        margin-top: 2px;
    }

    @media (max-width: 991px) {
        #column-qpb72cal9 {
        }
    }

    @media (max-width: 575px) {
        #column-qpb72cal9 {
        }
    }

    #addon-hihkn1g36 .row {
        margin-left: -10px;
        margin-right: -10px;
    }

    #addon-hihkn1g36 .row > div {
        padding-left: 10px;
        padding-right: 10px;
    }

    #row-ugg1c8gdt {
        margin-top: 51px;
    }

    @media (max-width: 991px) {
        #row-ugg1c8gdt {
        }
    }

    @media (max-width: 575px) {
        #row-ugg1c8gdt {
        }
    }

    #column-c4o9841vm {
    }

    @media (max-width: 991px) {
        #column-c4o9841vm {
        }
    }

    @media (max-width: 575px) {
        #column-c4o9841vm {
        }
    }

    #addon-o5c1kd3vz .pb-heading {
        color: ;
        text-align: center;
        font-size: 24px;
    }

    @media (max-width: 991px) {
        #addon-o5c1kd3vz .pb-heading {
            font-size: 22px;
        }
    }

    @media (max-width: 575px) {
        #addon-o5c1kd3vz .pb-heading {
            font-size: 22px;
        }
    }

    #column-i8r6yjj6q {
    }

    @media (max-width: 991px) {
        #column-i8r6yjj6q {
        }
    }

    @media (max-width: 575px) {
        #column-i8r6yjj6q {
        }
    }

    #column-ta7172iga {
        margin-top: 22px;
    }

    @media (max-width: 991px) {
        #column-ta7172iga {
        }
    }

    @media (max-width: 575px) {
        #column-ta7172iga {
        }
    }

    #column-48puem1g8 {
        margin-top: 2px;
    }

    @media (max-width: 991px) {
        #column-48puem1g8 {
        }
    }

    @media (max-width: 575px) {
        #column-48puem1g8 {
        }
    }

    #addon-uwv1niuuw .row {
        margin-left: -10px;
        margin-right: -10px;
    }

    #addon-uwv1niuuw .row > div {
        padding-left: 10px;
        padding-right: 10px;
    }

    #row-h8x1776es {
        margin-top: 67px;
    }

    @media (max-width: 991px) {
        #row-h8x1776es {
        }
    }

    @media (max-width: 575px) {
        #row-h8x1776es {
        }
    }

    #column-sru36d93w {
    }

    @media (max-width: 991px) {
        #column-sru36d93w {
        }
    }

    @media (max-width: 575px) {
        #column-sru36d93w {
        }
    }

    #addon-v0za8537a .pb-banner-text {
        top: 0px;
        left: 40px;
        text-align: left;
    }

    #addon-v0za8537a .pb-banner-subtitle {
        font-size: 14px;
        line-height: 21px;
        color: #ffffff
    }

    #addon-v0za8537a .pb-banner-title {
        font-size: 24px;
        line-height: 30px;
        color: #ffffff
    }

    #addon-v0za8537a .pb-banner-desc {
        font-size: 14px;
        line-height: 21px;
        color:
    }

    @media (max-width: 991px) {
        #addon-v0za8537a .pb-banner-text {
            top: 20px;
            left: 20px;
        }

        #addon-v0za8537a .pb-banner-subtitle {
            font-size: 14px;
            line-height: 21px;
        }

        #addon-v0za8537a .pb-banner-title {
            font-size: 24px;
            line-height: 30px;
        }

        #addon-v0za8537a .pb-banner-desc {
            font-size: 12px;
            line-height: 18px;
        }
    }

    @media (max-width: 575px) {
        #addon-v0za8537a .pb-banner-text {
            top: 20px;
            left: 20px;
        }

        #addon-v0za8537a .pb-banner-subtitle {
            font-size: 14px;
            line-height: 21px;
        }

        #addon-v0za8537a .pb-banner-title {
            font-size: 24px;
            line-height: 30px;
        }

        #addon-v0za8537a .pb-banner-desc {
            font-size: 10px;
            line-height: 15px;
        }
    }

    #column-95xbvfte1 {
    }

    @media (max-width: 991px) {
        #column-95xbvfte1 {
        }
    }

    @media (max-width: 575px) {
        #column-95xbvfte1 {
        }
    }

    #addon-zodu7zusf .pb-banner-text {
        top: 0px;
        left: 40px;
        text-align: left;
    }

    #addon-zodu7zusf .pb-banner-subtitle {
        font-size: 14px;
        line-height: 21px;
        color: #ffffff
    }

    #addon-zodu7zusf .pb-banner-title {
        font-size: 24px;
        line-height: 30px;
        color: #ffffff
    }

    #addon-zodu7zusf .pb-banner-desc {
        font-size: 14px;
        line-height: 21px;
        color:
    }

    @media (max-width: 991px) {
        #addon-zodu7zusf .pb-banner-text {
            top: 20px;
            left: 20px;
        }

        #addon-zodu7zusf .pb-banner-subtitle {
            font-size: 14px;
            line-height: 21px;
        }

        #addon-zodu7zusf .pb-banner-title {
            font-size: 24px;
            line-height: 30px;
        }

        #addon-zodu7zusf .pb-banner-desc {
            font-size: 12px;
            line-height: 18px;
        }
    }

    @media (max-width: 575px) {
        #addon-zodu7zusf .pb-banner-text {
            top: 20px;
            left: 20px;
        }

        #addon-zodu7zusf .pb-banner-subtitle {
            font-size: 14px;
            line-height: 21px;
        }

        #addon-zodu7zusf .pb-banner-title {
            font-size: 24px;
            line-height: 30px;
        }

        #addon-zodu7zusf .pb-banner-desc {
            font-size: 10px;
            line-height: 15px;
        }
    }

    #column-t4wjn72kp {
    }

    @media (max-width: 991px) {
        #column-t4wjn72kp {
        }
    }

    @media (max-width: 575px) {
        #column-t4wjn72kp {
        }
    }

    #addon-d632qyolq .pb-banner-text {
        top: 0px;
        left: 40px;
        text-align: left;
    }

    #addon-d632qyolq .pb-banner-subtitle {
        font-size: 14px;
        line-height: 21px;
        color: #ffffff
    }

    #addon-d632qyolq .pb-banner-title {
        font-size: 24px;
        line-height: 30px;
        color: #ffffff
    }

    #addon-d632qyolq .pb-banner-desc {
        font-size: 14px;
        line-height: 21px;
        color:
    }

    @media (max-width: 991px) {
        #addon-d632qyolq .pb-banner-text {
            top: 20px;
            left: 20px;
        }

        #addon-d632qyolq .pb-banner-subtitle {
            font-size: 14px;
            line-height: 21px;
        }

        #addon-d632qyolq .pb-banner-title {
            font-size: 24px;
            line-height: 30px;
        }

        #addon-d632qyolq .pb-banner-desc {
            font-size: 12px;
            line-height: 18px;
        }
    }

    @media (max-width: 575px) {
        #addon-d632qyolq .pb-banner-text {
            top: 20px;
            left: 20px;
        }

        #addon-d632qyolq .pb-banner-subtitle {
            font-size: 14px;
            line-height: 21px;
        }

        #addon-d632qyolq .pb-banner-title {
            font-size: 24px;
            line-height: 30px;
        }

        #addon-d632qyolq .pb-banner-desc {
            font-size: 10px;
            line-height: 15px;
        }
    }

    #row-obh9n1ceb {
        margin-top: 40px;
    }

    @media (max-width: 991px) {
        #row-obh9n1ceb {
        }
    }

    @media (max-width: 575px) {
        #row-obh9n1ceb {
        }
    }

    #column-a38iynsuw {
    }

    @media (max-width: 991px) {
        #column-a38iynsuw {
        }
    }

    @media (max-width: 575px) {
        #column-a38iynsuw {
        }
    }

    #addon-sh8v9eurj .pb-heading {
        color: ;
        text-align: center;
        font-size: 24px;
    }

    @media (max-width: 991px) {
        #addon-sh8v9eurj .pb-heading {
            font-size: 22px;
        }
    }

    @media (max-width: 575px) {
        #addon-sh8v9eurj .pb-heading {
            font-size: 22px;
        }
    }

    #column-zccnpjra1 {
    }

    @media (max-width: 991px) {
        #column-zccnpjra1 {
        }
    }

    @media (max-width: 575px) {
        #column-zccnpjra1 {
        }
    }

    #column-mdcq0qomx {
        margin-top: 3px;
    }

    @media (max-width: 991px) {
        #column-mdcq0qomx {
        }
    }

    @media (max-width: 575px) {
        #column-mdcq0qomx {
        }
    }

    #addon-64c94fifg .row {
        margin-left: -10px;
        margin-right: -10px;
    }

    #addon-64c94fifg .row > div {
        padding-left: 10px;
        padding-right: 10px;
    }

    #row-x91ezwue6 {
        background-color: #f8f8f8;
        padding-top: 46px;
        padding-bottom: 51px;
        margin-top: 30px;
    }

    @media (max-width: 991px) {
        #row-x91ezwue6 {
        }
    }

    @media (max-width: 575px) {
        #row-x91ezwue6 {
        }
    }

    #column-kvq8yps29 {
        margin-bottom: 25px;
    }

    @media (max-width: 991px) {
        #column-kvq8yps29 {
        }
    }

    @media (max-width: 575px) {
        #column-kvq8yps29 {
        }
    }

    #addon-lslgvhxfs .pb-heading {
        color: ;
        text-align: center;
        font-size: 24px;
    }

    @media (max-width: 991px) {
        #addon-lslgvhxfs .pb-heading {
            font-size: 22px;
        }
    }

    @media (max-width: 575px) {
        #addon-lslgvhxfs .pb-heading {
            font-size: 22px;
        }
    }

    #column-euwx3z980 {
    }

    @media (max-width: 991px) {
        #column-euwx3z980 {
        }
    }

    @media (max-width: 575px) {
        #column-euwx3z980 {
        }
    }

    #row-t1roqf33w {
        padding-top: 60px;
        padding-bottom: 70px;
    }

    @media (max-width: 991px) {
        #row-t1roqf33w {
        }
    }

    @media (max-width: 575px) {
        #row-t1roqf33w {
        }
    }

    #column-hgqhnftfg {
        margin-bottom: 14px;
    }

    @media (max-width: 991px) {
        #column-hgqhnftfg {
        }
    }

    @media (max-width: 575px) {
        #column-hgqhnftfg {
        }
    }

    #addon-th0cj01jl .gdz-icon-box {
        text-align: center;
    }

    #addon-th0cj01jl .gdz-icon-box i {
        font-size: 45px;
    }

    #addon-th0cj01jl .gdz-icon-box img {
        width: 25%;
    }

    #column-5xgr5me01 {
        margin-bottom: 5px;
    }

    @media (max-width: 991px) {
        #column-5xgr5me01 {
        }
    }

    @media (max-width: 575px) {
        #column-5xgr5me01 {
        }
    }

    #addon-srnbead6v .pb-heading {
        color: ;
        text-align: center;
        font-size: 24px;
    }

    @media (max-width: 991px) {
        #addon-srnbead6v .pb-heading {
            font-size: 22px;
        }
    }

    @media (max-width: 575px) {
        #addon-srnbead6v .pb-heading {
            font-size: 22px;
        }
    }

    #column-rv50wu6id {
        margin-bottom: 9px;
    }

    @media (max-width: 991px) {
        #column-rv50wu6id {
        }
    }

    @media (max-width: 575px) {
        #column-rv50wu6id {
        }
    }

    #addon-dnvw91jnk .pb-text-content {
        color: ;
        text-align: center;
        font-size: 14px;
    }

    @media (max-width: 991px) {
        #addon-dnvw91jnk .pb-text-content {
            font-size: 14px;
        }
    }

    @media (max-width: 575px) {
        #addon-dnvw91jnk .pb-text-content {
            font-size: 14px;
        }
    }

    #column-yfhwli0z7 {
    }

    @media (max-width: 991px) {
        #column-yfhwli0z7 {
        }
    }

    @media (max-width: 575px) {
        #column-yfhwli0z7 {
        }
    }
</style>
<div id="row-i5oou8rez" class="gdz-row ">
    <div class="container">
        <div class="row">
            <div id="column-rifrtujuf" class="layout-column col-lg-12 col-md-12 col-12 ">
                <div id="addon-xn7737f39" class="addon-box">
                    <div class="pb-spacer">
                        <div class="pb-spacer-inner"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%--<div id="row-cbgtkl9u3" class="gdz-row banner-overlay-dark">
    <div class="container">
        <div class="row">
            <div id="column-swsitjal1" class="layout-column col-lg-6 col-md-12 col-12 mb-2 mb-md-0">
                <div id="addon-ce6c4eqdc" class="addon-box">
                    <div class="pb-banner">
                        <a href="#" target="">
                            <div class="pb-banner-img">
			<span>
				<img src="assets/banner-1%20(1).jpg" alt="Image Alt" class="img-responsive"/>
			</span>
                            </div>
                            <div class="pb-banner-text pb-banner-center-left left">
                                <span class="pb-banner-subtitle">Your Guide To The World</span>            <h5
                                    class="pb-banner-title">Must-Read <br>Travel Books</h5>
                                <div class="pb-banner-button btn"><span>Find out more</span><i
                                        class="icon-long-arrow-right"></i></div>
                            </div>
                        </a>
                    </div>

                </div>
            </div>
            <div id="column-agi747c3c" class="layout-column col-lg-3 col-md-6 col-12 mb-2 mb-md-0">
                <div id="addon-9uvpqfyna" class="addon-box">
                    <div class="pb-banner banner-content-stretch">
                        <a href="#" target="">
                            <div class="pb-banner-img">
			<span>
				<img src="assets/banner-2%20(1).jpg" alt="Image Alt" class="img-responsive"/>
			</span>
                            </div>
                            <div class="pb-banner-text pb-banner-top-left left">
                                <span class="pb-banner-subtitle">New This Week</span>            <h5
                                    class="pb-banner-title">Discover Our <br>Best Romance <br>Books</h5>
                                <div class="pb-banner-button btn"><span>Discover now</span><i
                                        class="icon-long-arrow-right"></i></div>
                            </div>
                        </a>
                    </div>

                </div>
            </div>
            <div id="column-o4xhr90y6" class="layout-column col-lg-3 col-md-6 col-12 ">
                <div id="addon-77nx3txla" class="addon-box">
                    <div class="pb-banner">
                        <a href="#" target="">
                            <div class="pb-banner-img">
			<span>
				<img src="assets/banner-3.jpg" alt="Image Alt" class="img-responsive"/>
			</span>
                            </div>
                            <div class="pb-banner-text pb-banner-center-left left">
                                <span class="pb-banner-subtitle">Deal Of The Day</span>            <h5
                                    class="pb-banner-title">20% Off Use <br>Code: <span>mybook</span></h5>
                                <div class="pb-banner-button btn"><span>Shop now</span><i
                                        class="icon-long-arrow-right"></i></div>
                            </div>
                        </a>
                    </div>

                </div>
                <div id="addon-kknbu0r4f" class="addon-box">
                    <div class="pb-spacer">
                        <div class="pb-spacer-inner"></div>
                    </div>
                </div>
                <div id="addon-9hybhk8fh" class="addon-box">
                    <div class="pb-banner">
                        <a href="#" target="">
                            <div class="pb-banner-img">
			<span>
				<img src="assets/banner-4.jpg" alt="Image Alt" class="img-responsive"/>
			</span>
                            </div>
                            <div class="pb-banner-text pb-banner-center-left left">
                                <span class="pb-banner-subtitle">New Arrivals</span>            <h5
                                    class="pb-banner-title">Business <br> Economics</h5>
                                <div class="pb-banner-button btn"><span>Discover now</span><i
                                        class="icon-long-arrow-right"></i></div>
                            </div>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>--%>
<%--<div id="row-6lllqhkxu" class="gdz-row ">
    <div class="container">
        <div class="row">
            <div id="column-4lfbrt40h" class="layout-column col-lg-3 col-md-6 col-12 ">
                <div id="addon-5rhyxkq7p" class="addon-box">
                    <div class="pb-service-box">
                        <div class="pb-service-icon">
                            <i class="icon-truck"></i>
                        </div>
                        <div class="pb-service-content">
                            <h5 class="pb-service-title">Payment & Delivery</h5>
                            <div class="pb-service-description">
                                <p>Free shipping for orders over $50</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="column-2y1kdp964" class="layout-column col-lg-3 col-md-6 col-12 ">
                <div id="addon-rrcffktlp" class="addon-box">
                    <div class="pb-service-box">
                        <div class="pb-service-icon">
                            <i class="icon-rotate-left"></i>
                        </div>
                        <div class="pb-service-content">
                            <h5 class="pb-service-title">Return & Refund</h5>
                            <div class="pb-service-description">
                                <p>Free 100% money back guarantee</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="column-rma1buzfv" class="layout-column col-lg-3 col-md-6 col-12 ">
                <div id="addon-i5j08rk6d" class="addon-box">
                    <div class="pb-service-box">
                        <div class="pb-service-icon">
                            <i class="icon-life-ring"></i>
                        </div>
                        <div class="pb-service-content">
                            <h5 class="pb-service-title">Quality Support</h5>
                            <div class="pb-service-description">
                                <p>Alway online feedback 24/7</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="column-acjkqul12" class="layout-column col-lg-3 col-md-6 col-12 ">
                <div id="addon-381rl0da0" class="addon-box">
                    <div class="pb-service-box">
                        <div class="pb-service-icon">
                            <i class="icon-envelope"></i>
                        </div>
                        <div class="pb-service-content">
                            <h5 class="pb-service-title">Join Our Newsletter</h5>
                            <div class="pb-service-description">
                                <p>10% off by subscribing to our newsletter</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>--%>
<div id="row-dlwmhtm0z" class="gdz-row productbox-carousel best-seller">
    <div class="container">
        <div class="row">
            <div id="column-i8iorhy4b" class="layout-column col-lg-9 col-md-12 col-12 ">
                <div id="addon-eeieeg3ez" class="addon-box">
                    <h5 class="pb-heading title text-lg-left">
                        Mới cập nhật
                    </h5>
                </div>
            </div>
            <div id="column-79i6ye0wq" class="layout-column col-lg-3 col-md-12 col-12 ">
                <div id="addon-xaqeprna0" class="addon-box">
                    <div class="pb-button pb-button-right">
                        <a class=" btn btn-sm btn-link btn-right"
                           href="index.php?id_category=54&controller=category&id_lang=1" target="">
                            <span>Xem tất cả sách </span>
                            <i class="icon-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div id="column-qpb72cal9" class="layout-column col-lg-12 col-md-12 col-12 ">
                <div id="addon-hihkn1g36" class="addon-box">
                    <div class="pb-filter-products">
                        <div class="filter-products owl-carousel customs" data-items="5" data-lg="5" data-md="5"
                             data-sm="3" data-xs="2" data-nav="false" data-dots="true" data-auto="false"
                             data-rewind="false" data-slidebypage="1" data-margin="20">
                                <% ArrayList<MyObject> books = DB.getData("select books.*, authors.name as author_name, genre.name as genre_name from books inner join authors on books.author_id = authors.id inner join genre on books.genre_id = genre.id", new String[]{"id", "title", "description", "author_id", "genre_id", "quantity", "cover_image","price", "soft_file", "available", "author_name", "genre_name"}); %>
                                <% for (int i = 0; i < books.size(); i++) { %>
                                        <div class="item">
                                <div class="product-miniature js-product-miniature thumbnail-container productbox-12 customs"
                                     data-id-product="129" data-id-product-attribute="0" itemscope
                                     itemtype="http://schema.org/Product">
                                    <div class="product-preview">
                                        <a href="https://prestashop17.joommasters.com/molla/index.php?id_product=129&amp;rewrite=becoming&amp;controller=product&amp;id_lang=1"
                                           class="product-image blur-image">
                                            <img class="img-responsive product-img1"
                                                 data-src="${pageContext.request.contextPath}<%= books.get(i).cover_image %>"
                                                 src="${pageContext.request.contextPath}<%= books.get(i).cover_image %>"
                                                 alt=""
                                                 title="<%= books.get(i).title %>"
                                                 data-full-size-image-url="https://prestashop17.joommasters.com/molla/img/p/5/8/1/581-large_default.jpg"
                                            />
                                        </a>

                                    </div>
                                    <div class="product-info">

                                        <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                           onclick="WishlistCart('wishlist_block_list', 'add', '129', false, 1); return false;"
                                           data-id-product="129"></a>
                                        <a class="category-name"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?id_category=55&controller=category&id_lang=1">
                                            by
                                            <span><%=books.get(i).author_name%></span>
                                        </a>

                                        <h3 class="product-title" itemprop="name"><a class="product-link"
                                                                                     href="https://prestashop17.joommasters.com/molla/index.php?id_product=129&amp;rewrite=becoming&amp;controller=product&amp;id_lang=1"><%= books.get(i).title %></a>
                                        </h3>


                                        <div class="content_price">

                        <span class="price new has-discount">
                            $30.88
                        </span>

                                            <span class="old price">Was $32.50</span>


                                        </div>

                                        <div class="product-short-desc">
                                            <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae
                                                luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing.
                                                Sed lectus.</p>
                                        </div>
                                        <div class="product-footer">

                                            <div id="review">
                                                <div class="product-list-reviews no-review" data-id="129"
                                                     data-url="https://prestashop17.joommasters.com/molla/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
                                                    <div class="grade-stars small-stars">2</div>
                                                    <div class="comments-nb ratings-text"></div>
                                                </div>
                                            </div>


                                            <div class="product-action">
                                                <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                                   title="Add to cart" data-id-product="129" data-minimal-quantity="1"
                                                   data-token="588a5a1eb02312add78ccb857e952842">
                                                    <span>Add to cart</span>
                                                </a>
                                            </div>

                                            <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                               onclick="WishlistCart('wishlist_block_list', 'add', '129', false, 1); return false;"
                                               data-id-product="129">
                                                <span>Add to Wishlist</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="product-actions">

                                        <div class="content_price">

                                            <span class="price new has-discount">$30.88</span>

                                            <span class="old price">$32.50</span>


                                        </div>

                                        <a href="#" data-link-action="quickview" title="Quick View"
                                           class="btn-icon quick-view">
                                            <span>Quick View</span>
                                        </a>
                                        <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                           title="Add to cart" data-id-product="129" data-minimal-quantity="1"
                                           data-token="588a5a1eb02312add78ccb857e952842">
                                            <span>Add to cart</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                                <% } %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="row-ugg1c8gdt" class="gdz-row ">
    <div class="container">
        <div class="row">
            <div id="column-c4o9841vm" class="layout-column col-lg-9 col-md-12 col-12 ">
                <div id="addon-o5c1kd3vz" class="addon-box">
                    <h5 class="pb-heading title text-lg-left">
                        New Releases
                    </h5>
                </div>
            </div>
            <div id="column-i8r6yjj6q" class="layout-column col-lg-3 col-md-12 col-12 ">
                <div id="addon-mmnhodn91" class="addon-box">
                    <div class="pb-button pb-button-right">
                        <a class=" btn btn-sm btn-link btn-right"
                           href="index.php?id_category=54&controller=category&id_lang=1" target="">
                            <span>View more Products </span>
                            <i class="icon-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div id="column-ta7172iga" class="layout-column col-lg-4 col-md-6 col-12 banner_carousel">
                <div id="addon-5ivxb0q7f" class="addon-box">
                    <div class="pb-image-carousel">
                        <div class="image-carousel owl-carousel" data-items="1" data-lg="1" data-md="1" data-sm="1"
                             data-xs="1" data-nav="false" data-dots="true" data-auto="false" data-rewind="false"
                             data-slidebypage="1" data-margin="0">
                            <div class="image-item">
                                <a href="#" alt="Image Alt">
                                    <img src="assets/banner-9.jpg"/>
                                </a>
                            </div>
                            <div class="image-item">
                                <a href="#" alt="Image Alt">
                                    <img src="assets/banner-10.jpg"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="column-48puem1g8" class="layout-column col-lg-8 col-md-6 col-12 product_carousel">
                <div id="addon-uwv1niuuw" class="addon-box">
                    <div class="pb-filter-products">
                        <div class="filter-products owl-carousel customs" data-items="4" data-lg="4" data-md="4"
                             data-sm="3" data-xs="2" data-nav="false" data-dots="false" data-auto="false"
                             data-rewind="false" data-slidebypage="1" data-margin="20">
                            <div class="item">
                                <div class="product-miniature js-product-miniature thumbnail-container productbox-12 customs"
                                     data-id-product="143" data-id-product-attribute="0" itemscope
                                     itemtype="http://schema.org/Product">
                                    <div class="product-preview">

                                        <a href="https://prestashop17.joommasters.com/molla/index.php?id_product=143&amp;rewrite=this-is-going-to-hurt-secret-diaries-of-a-juni&amp;controller=product&amp;id_lang=1"
                                           class="product-image blur-image">
                                            <img class="img-responsive product-img1"
                                                 data-src="https://prestashop17.joommasters.com/molla/img/p/6/0/9/609-home_default_200x310.jpg"
                                                 src="https://prestashop17.joommasters.com/molla/img/p/6/0/9/609-home_default_200x310.jpg"
                                                 alt=""
                                                 title="This is Going to Hurt: Secret Diaries of a Juni"
                                                 data-full-size-image-url="https://prestashop17.joommasters.com/molla/img/p/6/0/9/609-large_default.jpg"
                                            />
                                        </a>


                                        <ul class="product-flags">
                                        </ul>

                                    </div>
                                    <div class="product-info">

                                        <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                           onclick="WishlistCart('wishlist_block_list', 'add', '143', false, 1); return false;"
                                           data-id-product="143"></a>
                                        <a class="category-name"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?id_category=61&controller=category&id_lang=1">
                                            by
                                            <span>John Gray</span>
                                        </a>

                                        <h3 class="product-title" itemprop="name"><a class="product-link"
                                                                                     href="https://prestashop17.joommasters.com/molla/index.php?id_product=143&amp;rewrite=this-is-going-to-hurt-secret-diaries-of-a-juni&amp;controller=product&amp;id_lang=1">This
                                            is Going to Hurt: Secret...</a></h3>


                                        <div class="content_price">

						<span class="price new ">
							$12.00
						</span>


                                        </div>

                                        <div class="product-short-desc">
                                            <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae
                                                luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing.
                                                Sed lectus.</p>
                                        </div>
                                        <div class="product-footer">

                                            <div id="review">
                                                <div class="product-list-reviews no-review" data-id="143"
                                                     data-url="https://prestashop17.joommasters.com/molla/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
                                                    <div class="grade-stars small-stars"></div>
                                                    <div class="comments-nb ratings-text"></div>
                                                </div>
                                            </div>


                                            <div class="product-action">
                                                <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                                   title="Add to cart" data-id-product="143" data-minimal-quantity="1"
                                                   data-token="588a5a1eb02312add78ccb857e952842">
                                                    <span>Add to cart</span>
                                                </a>
                                            </div>

                                            <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                               onclick="WishlistCart('wishlist_block_list', 'add', '143', false, 1); return false;"
                                               data-id-product="143">
                                                <span>Add to Wishlist</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="product-actions">

                                        <div class="content_price">

                                            <span class="price new ">$12.00</span>


                                        </div>

                                        <a href="#" data-link-action="quickview" title="Quick View"
                                           class="btn-icon quick-view">
                                            <span>Quick View</span>
                                        </a>
                                        <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                           title="Add to cart" data-id-product="143" data-minimal-quantity="1"
                                           data-token="588a5a1eb02312add78ccb857e952842">
                                            <span>Add to cart</span>
                                        </a>
                                    </div>
                                </div>

                            </div>
                            <div class="item">
                                <div class="product-miniature js-product-miniature thumbnail-container productbox-12 customs"
                                     data-id-product="142" data-id-product-attribute="0" itemscope
                                     itemtype="http://schema.org/Product">
                                    <div class="product-preview">

                                        <a href="https://prestashop17.joommasters.com/molla/index.php?id_product=142&amp;rewrite=moneyland-why-thieves-and-crooks-now-rule-&amp;controller=product&amp;id_lang=1"
                                           class="product-image blur-image">
                                            <img class="img-responsive product-img1"
                                                 data-src="https://prestashop17.joommasters.com/molla/img/p/6/0/7/607-home_default_200x310.jpg"
                                                 src="https://prestashop17.joommasters.com/molla/img/p/6/0/7/607-home_default_200x310.jpg"
                                                 alt=""
                                                 title="Moneyland: Why Thieves And Crooks Now Rule ..."
                                                 data-full-size-image-url="https://prestashop17.joommasters.com/molla/img/p/6/0/7/607-large_default.jpg"
                                            />
                                        </a>


                                        <ul class="product-flags">
                                        </ul>

                                    </div>
                                    <div class="product-info">

                                        <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                           onclick="WishlistCart('wishlist_block_list', 'add', '142', false, 1); return false;"
                                           data-id-product="142"></a>
                                        <a class="category-name"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?id_category=61&controller=category&id_lang=1">
                                            by
                                            <span>John Gray</span>
                                        </a>

                                        <h3 class="product-title" itemprop="name"><a class="product-link"
                                                                                     href="https://prestashop17.joommasters.com/molla/index.php?id_product=142&amp;rewrite=moneyland-why-thieves-and-crooks-now-rule-&amp;controller=product&amp;id_lang=1">Moneyland:
                                            Why Thieves And...</a></h3>


                                        <div class="content_price">

						<span class="price new ">
							$12.99
						</span>


                                        </div>

                                        <div class="product-short-desc">
                                            <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae
                                                luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing.
                                                Sed lectus.</p>
                                        </div>
                                        <div class="product-footer">

                                            <div id="review">
                                                <div class="product-list-reviews no-review" data-id="142"
                                                     data-url="https://prestashop17.joommasters.com/molla/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
                                                    <div class="grade-stars small-stars"></div>
                                                    <div class="comments-nb ratings-text"></div>
                                                </div>
                                            </div>


                                            <div class="product-action">
                                                <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                                   title="Add to cart" data-id-product="142" data-minimal-quantity="1"
                                                   data-token="588a5a1eb02312add78ccb857e952842">
                                                    <span>Add to cart</span>
                                                </a>
                                            </div>

                                            <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                               onclick="WishlistCart('wishlist_block_list', 'add', '142', false, 1); return false;"
                                               data-id-product="142">
                                                <span>Add to Wishlist</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="product-actions">

                                        <div class="content_price">

                                            <span class="price new ">$12.99</span>


                                        </div>

                                        <a href="#" data-link-action="quickview" title="Quick View"
                                           class="btn-icon quick-view">
                                            <span>Quick View</span>
                                        </a>
                                        <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                           title="Add to cart" data-id-product="142" data-minimal-quantity="1"
                                           data-token="588a5a1eb02312add78ccb857e952842">
                                            <span>Add to cart</span>
                                        </a>
                                    </div>
                                </div>

                            </div>
                            <div class="item">
                                <div class="product-miniature js-product-miniature thumbnail-container productbox-12 customs"
                                     data-id-product="141" data-id-product-attribute="0" itemscope
                                     itemtype="http://schema.org/Product">
                                    <div class="product-preview">

                                        <a href="https://prestashop17.joommasters.com/molla/index.php?id_product=141&amp;rewrite=the-librarian-of-auschwitz&amp;controller=product&amp;id_lang=1"
                                           class="product-image blur-image">
                                            <img class="img-responsive product-img1"
                                                 data-src="https://prestashop17.joommasters.com/molla/img/p/6/0/5/605-home_default_200x310.jpg"
                                                 src="https://prestashop17.joommasters.com/molla/img/p/6/0/5/605-home_default_200x310.jpg"
                                                 alt=""
                                                 title="The Librarian of Auschwitz"
                                                 data-full-size-image-url="https://prestashop17.joommasters.com/molla/img/p/6/0/5/605-large_default.jpg"
                                            />
                                        </a>


                                        <ul class="product-flags">
                                        </ul>

                                    </div>
                                    <div class="product-info">

                                        <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                           onclick="WishlistCart('wishlist_block_list', 'add', '141', false, 1); return false;"
                                           data-id-product="141"></a>
                                        <a class="category-name"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?id_category=61&controller=category&id_lang=1">
                                            by
                                            <span>John Gray</span>
                                        </a>

                                        <h3 class="product-title" itemprop="name"><a class="product-link"
                                                                                     href="https://prestashop17.joommasters.com/molla/index.php?id_product=141&amp;rewrite=the-librarian-of-auschwitz&amp;controller=product&amp;id_lang=1">The
                                            Librarian of Auschwitz</a></h3>


                                        <div class="content_price">

						<span class="price new ">
							$10.99
						</span>


                                        </div>

                                        <div class="product-short-desc">
                                            <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae
                                                luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing.
                                                Sed lectus.</p>
                                        </div>
                                        <div class="product-footer">

                                            <div id="review">
                                                <div class="product-list-reviews no-review" data-id="141"
                                                     data-url="https://prestashop17.joommasters.com/molla/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
                                                    <div class="grade-stars small-stars"></div>
                                                    <div class="comments-nb ratings-text"></div>
                                                </div>
                                            </div>


                                            <div class="product-action">
                                                <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                                   title="Add to cart" data-id-product="141" data-minimal-quantity="1"
                                                   data-token="588a5a1eb02312add78ccb857e952842">
                                                    <span>Add to cart</span>
                                                </a>
                                            </div>

                                            <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                               onclick="WishlistCart('wishlist_block_list', 'add', '141', false, 1); return false;"
                                               data-id-product="141">
                                                <span>Add to Wishlist</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="product-actions">

                                        <div class="content_price">

                                            <span class="price new ">$10.99</span>


                                        </div>

                                        <a href="#" data-link-action="quickview" title="Quick View"
                                           class="btn-icon quick-view">
                                            <span>Quick View</span>
                                        </a>
                                        <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                           title="Add to cart" data-id-product="141" data-minimal-quantity="1"
                                           data-token="588a5a1eb02312add78ccb857e952842">
                                            <span>Add to cart</span>
                                        </a>
                                    </div>
                                </div>

                            </div>
                            <div class="item">
                                <div class="product-miniature js-product-miniature thumbnail-container productbox-12 customs"
                                     data-id-product="140" data-id-product-attribute="0" itemscope
                                     itemtype="http://schema.org/Product">
                                    <div class="product-preview">

                                        <a href="https://prestashop17.joommasters.com/molla/index.php?id_product=140&amp;rewrite=you-got-this&amp;controller=product&amp;id_lang=1"
                                           class="product-image blur-image">
                                            <img class="img-responsive product-img1"
                                                 data-src="https://prestashop17.joommasters.com/molla/img/p/6/0/3/603-home_default_200x310.jpg"
                                                 src="https://prestashop17.joommasters.com/molla/img/p/6/0/3/603-home_default_200x310.jpg"
                                                 alt=""
                                                 title="You Got This"
                                                 data-full-size-image-url="https://prestashop17.joommasters.com/molla/img/p/6/0/3/603-large_default.jpg"
                                            />
                                        </a>


                                        <ul class="product-flags">
                                        </ul>

                                    </div>
                                    <div class="product-info">

                                        <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                           onclick="WishlistCart('wishlist_block_list', 'add', '140', false, 1); return false;"
                                           data-id-product="140"></a>
                                        <a class="category-name"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?id_category=61&controller=category&id_lang=1">
                                            by
                                            <span>John Gray</span>
                                        </a>

                                        <h3 class="product-title" itemprop="name"><a class="product-link"
                                                                                     href="https://prestashop17.joommasters.com/molla/index.php?id_product=140&amp;rewrite=you-got-this&amp;controller=product&amp;id_lang=1">You
                                            Got This</a></h3>


                                        <div class="content_price">

						<span class="price new ">
							$7.99
						</span>


                                        </div>

                                        <div class="product-short-desc">
                                            <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae
                                                luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing.
                                                Sed lectus.</p>
                                        </div>
                                        <div class="product-footer">

                                            <div id="review">
                                                <div class="product-list-reviews no-review" data-id="140"
                                                     data-url="https://prestashop17.joommasters.com/molla/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
                                                    <div class="grade-stars small-stars"></div>
                                                    <div class="comments-nb ratings-text"></div>
                                                </div>
                                            </div>


                                            <div class="product-action">
                                                <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                                   title="Add to cart" data-id-product="140" data-minimal-quantity="1"
                                                   data-token="588a5a1eb02312add78ccb857e952842">
                                                    <span>Add to cart</span>
                                                </a>
                                            </div>

                                            <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                               onclick="WishlistCart('wishlist_block_list', 'add', '140', false, 1); return false;"
                                               data-id-product="140">
                                                <span>Add to Wishlist</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="product-actions">

                                        <div class="content_price">

                                            <span class="price new ">$7.99</span>


                                        </div>

                                        <a href="#" data-link-action="quickview" title="Quick View"
                                           class="btn-icon quick-view">
                                            <span>Quick View</span>
                                        </a>
                                        <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                           title="Add to cart" data-id-product="140" data-minimal-quantity="1"
                                           data-token="588a5a1eb02312add78ccb857e952842">
                                            <span>Add to cart</span>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="row-h8x1776es" class="gdz-row banner-group banner-overlay-dark">
    <div class="container">
        <div class="row">
            <div id="column-sru36d93w" class="layout-column col-lg-4 col-md-6 col-12 mb-2 mb-lg-0">
                <div id="addon-v0za8537a" class="addon-box">
                    <div class="pb-banner">
                        <a href="#" target="">
                            <div class="pb-banner-img">
			<span>
				<img src="assets/banner-6.jpg" alt="Image Alt" class="img-responsive"/>
			</span>
                            </div>
                            <div class="pb-banner-text pb-banner-center-left left">
                                <span class="pb-banner-subtitle">A Perfect Choice For Your Children</span>
                                <h5 class="pb-banner-title">Children's <br>Bestselling Books</h5>
                                <div class="pb-banner-button btn"><span>Discover now</span><i
                                        class="icon-long-arrow-right"></i></div>
                            </div>
                        </a>
                    </div>

                </div>
            </div>
            <div id="column-95xbvfte1" class="layout-column col-lg-4 col-md-6 col-12 mb-2 mb-lg-0">
                <div id="addon-zodu7zusf" class="addon-box">
                    <div class="pb-banner">
                        <a href="#" target="">
                            <div class="pb-banner-img">
			<span>
				<img src="assets/banner-7.jpg" alt="Image Alt" class="img-responsive"/>
			</span>
                            </div>
                            <div class="pb-banner-text pb-banner-center-left left">
                                <span class="pb-banner-subtitle">Mental Health Awareness Week</span>            <h5
                                    class="pb-banner-title">Self-Help For <br>Your Future.</h5>
                                <div class="pb-banner-button btn"><span>Discover now</span><i
                                        class="icon-long-arrow-right"></i></div>
                            </div>
                        </a>
                    </div>

                </div>
            </div>
            <div id="column-t4wjn72kp" class="layout-column col-lg-4 col-md-6 col-12 ">
                <div id="addon-d632qyolq" class="addon-box">
                    <div class="pb-banner">
                        <a href="#" target="">
                            <div class="pb-banner-img">
			<span>
				<img src="assets/banner-8.jpg" alt="Image Alt" class="img-responsive"/>
			</span>
                            </div>
                            <div class="pb-banner-text pb-banner-center-left left">
                                <span class="pb-banner-subtitle">New York Times Bestsellers</span>            <h5
                                    class="pb-banner-title">Bestselling Food <br>and Drink Books.</h5>
                                <div class="pb-banner-button btn"><span>Discover now</span><i
                                        class="icon-long-arrow-right"></i></div>
                            </div>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div id="row-obh9n1ceb" class="gdz-row productbox-carousel">
    <div class="container">
        <div class="row">
            <div id="column-a38iynsuw" class="layout-column col-lg-9 col-md-12 col-12 ">
                <div id="addon-sh8v9eurj" class="addon-box">
                    <h5 class="pb-heading title text-lg-left">
                        Picks From Our Experts
                    </h5>
                </div>
            </div>
            <div id="column-zccnpjra1" class="layout-column col-lg-3 col-md-12 col-12 ">
                <div id="addon-m2kbt3w0v" class="addon-box">
                    <div class="pb-button pb-button-right">
                        <a class=" btn btn-sm btn-link btn-right"
                           href="index.php?id_category=54&controller=category&id_lang=1" target="">
                            <span>View more Products </span>
                            <i class="icon-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div id="column-mdcq0qomx" class="layout-column col-lg-12 col-md-12 col-12 ">
                <div id="addon-64c94fifg" class="addon-box">
                    <div class="pb-filter-products">
                        <div class="filter-products owl-carousel customs" data-items="6" data-lg="6" data-md="6"
                             data-sm="3" data-xs="2" data-nav="false" data-dots="false" data-auto="false"
                             data-rewind="false" data-slidebypage="1" data-margin="20">
                            <div class="item">
                                <div class="product-miniature js-product-miniature thumbnail-container productbox-12 customs"
                                     data-id-product="129" data-id-product-attribute="0" itemscope
                                     itemtype="http://schema.org/Product">
                                    <div class="product-preview">

                                        <a href="https://prestashop17.joommasters.com/molla/index.php?id_product=129&amp;rewrite=becoming&amp;controller=product&amp;id_lang=1"
                                           class="product-image blur-image">
                                            <img class="img-responsive product-img1"
                                                 data-src="https://prestashop17.joommasters.com/molla/img/p/5/8/1/581-home_default_200x310.jpg"
                                                 src="https://prestashop17.joommasters.com/molla/img/p/5/8/1/581-home_default_200x310.jpg"
                                                 alt=""
                                                 title="Becoming"
                                                 data-full-size-image-url="https://prestashop17.joommasters.com/molla/img/p/5/8/1/581-large_default.jpg"
                                            />
                                        </a>


                                        <ul class="product-flags">
                                            <li class="product-flag on-sale">On sale</li>
                                            <li class="product-flag discount">-5%</li>
                                        </ul>

                                    </div>
                                    <div class="product-info">

                                        <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                           onclick="WishlistCart('wishlist_block_list', 'add', '129', false, 1); return false;"
                                           data-id-product="129"></a>
                                        <a class="category-name"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?id_category=55&controller=category&id_lang=1">
                                            by
                                            <span>Michelle Obama</span>
                                        </a>

                                        <h3 class="product-title" itemprop="name"><a class="product-link"
                                                                                     href="https://prestashop17.joommasters.com/molla/index.php?id_product=129&amp;rewrite=becoming&amp;controller=product&amp;id_lang=1">Becoming</a>
                                        </h3>


                                        <div class="content_price">

						<span class="price new has-discount">
							$30.88
						</span>

                                            <span class="old price">Was $32.50</span>


                                        </div>

                                        <div class="product-short-desc">
                                            <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae
                                                luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing.
                                                Sed lectus.</p>
                                        </div>
                                        <div class="product-footer">

                                            <div id="review">
                                                <div class="product-list-reviews no-review" data-id="129"
                                                     data-url="https://prestashop17.joommasters.com/molla/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
                                                    <div class="grade-stars small-stars"></div>
                                                    <div class="comments-nb ratings-text"></div>
                                                </div>
                                            </div>


                                            <div class="product-action">
                                                <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                                   title="Add to cart" data-id-product="129" data-minimal-quantity="1"
                                                   data-token="588a5a1eb02312add78ccb857e952842">
                                                    <span>Add to cart</span>
                                                </a>
                                            </div>

                                            <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                               onclick="WishlistCart('wishlist_block_list', 'add', '129', false, 1); return false;"
                                               data-id-product="129">
                                                <span>Add to Wishlist</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="product-actions">

                                        <div class="content_price">

                                            <span class="price new has-discount">$30.88</span>

                                            <span class="old price">$32.50</span>


                                        </div>

                                        <a href="#" data-link-action="quickview" title="Quick View"
                                           class="btn-icon quick-view">
                                            <span>Quick View</span>
                                        </a>
                                        <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                           title="Add to cart" data-id-product="129" data-minimal-quantity="1"
                                           data-token="588a5a1eb02312add78ccb857e952842">
                                            <span>Add to cart</span>
                                        </a>
                                    </div>
                                </div>

                            </div>
                            <div class="item">
                                <div class="product-miniature js-product-miniature thumbnail-container productbox-12 customs"
                                     data-id-product="130" data-id-product-attribute="0" itemscope
                                     itemtype="http://schema.org/Product">
                                    <div class="product-preview">

                                        <a href="https://prestashop17.joommasters.com/molla/index.php?id_product=130&amp;rewrite=the-secret-barrister-stories-of-the-law-and-how-it-s-&amp;controller=product&amp;id_lang=1"
                                           class="product-image blur-image">
                                            <img class="img-responsive product-img1"
                                                 data-src="https://prestashop17.joommasters.com/molla/img/p/5/8/3/583-home_default_200x310.jpg"
                                                 src="https://prestashop17.joommasters.com/molla/img/p/5/8/3/583-home_default_200x310.jpg"
                                                 alt=""
                                                 title="The Secret Barrister: Stories of the Law and How It&#039;s ..."
                                                 data-full-size-image-url="https://prestashop17.joommasters.com/molla/img/p/5/8/3/583-large_default.jpg"
                                            />
                                        </a>


                                        <ul class="product-flags">
                                        </ul>

                                    </div>
                                    <div class="product-info">

                                        <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                           onclick="WishlistCart('wishlist_block_list', 'add', '130', false, 1); return false;"
                                           data-id-product="130"></a>
                                        <a class="category-name"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?id_category=56&controller=category&id_lang=1">
                                            by
                                            <span>Jordan B. Peterson</span>
                                        </a>

                                        <h3 class="product-title" itemprop="name"><a class="product-link"
                                                                                     href="https://prestashop17.joommasters.com/molla/index.php?id_product=130&amp;rewrite=the-secret-barrister-stories-of-the-law-and-how-it-s-&amp;controller=product&amp;id_lang=1">The
                                            Secret Barrister: Stories of...</a></h3>


                                        <div class="content_price">

						<span class="price new ">
							$17.34
						</span>


                                        </div>

                                        <div class="product-short-desc">
                                            <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae
                                                luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing.
                                                Sed lectus.</p>
                                        </div>
                                        <div class="product-footer">

                                            <div id="review">
                                                <div class="product-list-reviews no-review" data-id="130"
                                                     data-url="https://prestashop17.joommasters.com/molla/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
                                                    <div class="grade-stars small-stars"></div>
                                                    <div class="comments-nb ratings-text"></div>
                                                </div>
                                            </div>


                                            <div class="product-action">
                                                <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                                   title="Add to cart" data-id-product="130" data-minimal-quantity="1"
                                                   data-token="588a5a1eb02312add78ccb857e952842">
                                                    <span>Add to cart</span>
                                                </a>
                                            </div>

                                            <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                               onclick="WishlistCart('wishlist_block_list', 'add', '130', false, 1); return false;"
                                               data-id-product="130">
                                                <span>Add to Wishlist</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="product-actions">

                                        <div class="content_price">

                                            <span class="price new ">$17.34</span>


                                        </div>

                                        <a href="#" data-link-action="quickview" title="Quick View"
                                           class="btn-icon quick-view">
                                            <span>Quick View</span>
                                        </a>
                                        <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                           title="Add to cart" data-id-product="130" data-minimal-quantity="1"
                                           data-token="588a5a1eb02312add78ccb857e952842">
                                            <span>Add to cart</span>
                                        </a>
                                    </div>
                                </div>

                            </div>
                            <div class="item">
                                <div class="product-miniature js-product-miniature thumbnail-container productbox-12 customs"
                                     data-id-product="131" data-id-product-attribute="0" itemscope
                                     itemtype="http://schema.org/Product">
                                    <div class="product-preview">

                                        <a href="https://prestashop17.joommasters.com/molla/index.php?id_product=131&amp;rewrite=five-feet-apart&amp;controller=product&amp;id_lang=1"
                                           class="product-image blur-image">
                                            <img class="img-responsive product-img1"
                                                 data-src="https://prestashop17.joommasters.com/molla/img/p/5/8/5/585-home_default_200x310.jpg"
                                                 src="https://prestashop17.joommasters.com/molla/img/p/5/8/5/585-home_default_200x310.jpg"
                                                 alt=""
                                                 title="Five Feet Apart"
                                                 data-full-size-image-url="https://prestashop17.joommasters.com/molla/img/p/5/8/5/585-large_default.jpg"
                                            />
                                        </a>


                                        <ul class="product-flags">
                                            <li class="product-flag on-sale">On sale</li>
                                            <li class="product-flag discount">-10%</li>
                                        </ul>

                                    </div>
                                    <div class="product-info">

                                        <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                           onclick="WishlistCart('wishlist_block_list', 'add', '131', false, 1); return false;"
                                           data-id-product="131"></a>
                                        <a class="category-name"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?id_category=58&controller=category&id_lang=1">
                                            by
                                            <span>Rachael Lippincott</span>
                                        </a>

                                        <h3 class="product-title" itemprop="name"><a class="product-link"
                                                                                     href="https://prestashop17.joommasters.com/molla/index.php?id_product=131&amp;rewrite=five-feet-apart&amp;controller=product&amp;id_lang=1">Five
                                            Feet Apart</a></h3>


                                        <div class="content_price">

						<span class="price new has-discount">
							$17.09
						</span>

                                            <span class="old price">Was $18.99</span>


                                        </div>

                                        <div class="product-short-desc">
                                            <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae
                                                luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing.
                                                Sed lectus.</p>
                                        </div>
                                        <div class="product-footer">

                                            <div id="review">
                                                <div class="product-list-reviews no-review" data-id="131"
                                                     data-url="https://prestashop17.joommasters.com/molla/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
                                                    <div class="grade-stars small-stars"></div>
                                                    <div class="comments-nb ratings-text"></div>
                                                </div>
                                            </div>


                                            <div class="product-action">
                                                <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                                   title="Add to cart" data-id-product="131" data-minimal-quantity="1"
                                                   data-token="588a5a1eb02312add78ccb857e952842">
                                                    <span>Add to cart</span>
                                                </a>
                                            </div>

                                            <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                               onclick="WishlistCart('wishlist_block_list', 'add', '131', false, 1); return false;"
                                               data-id-product="131">
                                                <span>Add to Wishlist</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="product-actions">

                                        <div class="content_price">

                                            <span class="price new has-discount">$17.09</span>

                                            <span class="old price">$18.99</span>


                                        </div>

                                        <a href="#" data-link-action="quickview" title="Quick View"
                                           class="btn-icon quick-view">
                                            <span>Quick View</span>
                                        </a>
                                        <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                           title="Add to cart" data-id-product="131" data-minimal-quantity="1"
                                           data-token="588a5a1eb02312add78ccb857e952842">
                                            <span>Add to cart</span>
                                        </a>
                                    </div>
                                </div>

                            </div>
                            <div class="item">
                                <div class="product-miniature js-product-miniature thumbnail-container productbox-12 customs"
                                     data-id-product="132" data-id-product-attribute="0" itemscope
                                     itemtype="http://schema.org/Product">
                                    <div class="product-preview">

                                        <a href="https://prestashop17.joommasters.com/molla/index.php?id_product=132&amp;rewrite=one-of-us-is-lying&amp;controller=product&amp;id_lang=1"
                                           class="product-image blur-image">
                                            <img class="img-responsive product-img1"
                                                 data-src="https://prestashop17.joommasters.com/molla/img/p/5/8/7/587-home_default_200x310.jpg"
                                                 src="https://prestashop17.joommasters.com/molla/img/p/5/8/7/587-home_default_200x310.jpg"
                                                 alt=""
                                                 title="One of Us Is Lying"
                                                 data-full-size-image-url="https://prestashop17.joommasters.com/molla/img/p/5/8/7/587-large_default.jpg"
                                            />
                                        </a>


                                        <ul class="product-flags">
                                        </ul>

                                    </div>
                                    <div class="product-info">

                                        <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                           onclick="WishlistCart('wishlist_block_list', 'add', '132', false, 1); return false;"
                                           data-id-product="132"></a>
                                        <a class="category-name"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?id_category=59&controller=category&id_lang=1">
                                            by
                                            <span>Karen M. McManus</span>
                                        </a>

                                        <h3 class="product-title" itemprop="name"><a class="product-link"
                                                                                     href="https://prestashop17.joommasters.com/molla/index.php?id_product=132&amp;rewrite=one-of-us-is-lying&amp;controller=product&amp;id_lang=1">One
                                            of Us Is Lying</a></h3>


                                        <div class="content_price">

						<span class="price new ">
							$17.99
						</span>


                                        </div>

                                        <div class="product-short-desc">
                                            <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae
                                                luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing.
                                                Sed lectus.</p>
                                        </div>
                                        <div class="product-footer">

                                            <div id="review">
                                                <div class="product-list-reviews no-review" data-id="132"
                                                     data-url="https://prestashop17.joommasters.com/molla/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
                                                    <div class="grade-stars small-stars"></div>
                                                    <div class="comments-nb ratings-text"></div>
                                                </div>
                                            </div>


                                            <div class="product-action">
                                                <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                                   title="Add to cart" data-id-product="132" data-minimal-quantity="1"
                                                   data-token="588a5a1eb02312add78ccb857e952842">
                                                    <span>Add to cart</span>
                                                </a>
                                            </div>

                                            <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                               onclick="WishlistCart('wishlist_block_list', 'add', '132', false, 1); return false;"
                                               data-id-product="132">
                                                <span>Add to Wishlist</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="product-actions">

                                        <div class="content_price">

                                            <span class="price new ">$17.99</span>


                                        </div>

                                        <a href="#" data-link-action="quickview" title="Quick View"
                                           class="btn-icon quick-view">
                                            <span>Quick View</span>
                                        </a>
                                        <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                           title="Add to cart" data-id-product="132" data-minimal-quantity="1"
                                           data-token="588a5a1eb02312add78ccb857e952842">
                                            <span>Add to cart</span>
                                        </a>
                                    </div>
                                </div>

                            </div>
                            <div class="item">
                                <div class="product-miniature js-product-miniature thumbnail-container productbox-12 customs"
                                     data-id-product="133" data-id-product-attribute="0" itemscope
                                     itemtype="http://schema.org/Product">
                                    <div class="product-preview">

                                        <a href="https://prestashop17.joommasters.com/molla/index.php?id_product=133&amp;rewrite=how-to-stop-time&amp;controller=product&amp;id_lang=1"
                                           class="product-image blur-image">
                                            <img class="img-responsive product-img1"
                                                 data-src="https://prestashop17.joommasters.com/molla/img/p/5/8/9/589-home_default_200x310.jpg"
                                                 src="https://prestashop17.joommasters.com/molla/img/p/5/8/9/589-home_default_200x310.jpg"
                                                 alt=""
                                                 title="How to Stop Time"
                                                 data-full-size-image-url="https://prestashop17.joommasters.com/molla/img/p/5/8/9/589-large_default.jpg"
                                            />
                                        </a>


                                        <ul class="product-flags">
                                        </ul>

                                    </div>
                                    <div class="product-info">

                                        <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                           onclick="WishlistCart('wishlist_block_list', 'add', '133', false, 1); return false;"
                                           data-id-product="133"></a>
                                        <a class="category-name"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?id_category=60&controller=category&id_lang=1">
                                            by
                                            <span>Matt Haig</span>
                                        </a>

                                        <h3 class="product-title" itemprop="name"><a class="product-link"
                                                                                     href="https://prestashop17.joommasters.com/molla/index.php?id_product=133&amp;rewrite=how-to-stop-time&amp;controller=product&amp;id_lang=1">How
                                            to Stop Time</a></h3>


                                        <div class="content_price">

						<span class="price new ">
							$11.68
						</span>


                                        </div>

                                        <div class="product-short-desc">
                                            <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae
                                                luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing.
                                                Sed lectus.</p>
                                        </div>
                                        <div class="product-footer">

                                            <div id="review">
                                                <div class="product-list-reviews no-review" data-id="133"
                                                     data-url="https://prestashop17.joommasters.com/molla/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
                                                    <div class="grade-stars small-stars"></div>
                                                    <div class="comments-nb ratings-text"></div>
                                                </div>
                                            </div>


                                            <div class="product-action">
                                                <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                                   title="Add to cart" data-id-product="133" data-minimal-quantity="1"
                                                   data-token="588a5a1eb02312add78ccb857e952842">
                                                    <span>Add to cart</span>
                                                </a>
                                            </div>

                                            <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                               onclick="WishlistCart('wishlist_block_list', 'add', '133', false, 1); return false;"
                                               data-id-product="133">
                                                <span>Add to Wishlist</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="product-actions">

                                        <div class="content_price">

                                            <span class="price new ">$11.68</span>


                                        </div>

                                        <a href="#" data-link-action="quickview" title="Quick View"
                                           class="btn-icon quick-view">
                                            <span>Quick View</span>
                                        </a>
                                        <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                           title="Add to cart" data-id-product="133" data-minimal-quantity="1"
                                           data-token="588a5a1eb02312add78ccb857e952842">
                                            <span>Add to cart</span>
                                        </a>
                                    </div>
                                </div>

                            </div>
                            <div class="item">
                                <div class="product-miniature js-product-miniature thumbnail-container productbox-12 customs"
                                     data-id-product="134" data-id-product-attribute="0" itemscope
                                     itemtype="http://schema.org/Product">
                                    <div class="product-preview">

                                        <a href="https://prestashop17.joommasters.com/molla/index.php?id_product=134&amp;rewrite=the-durrells-of-corfu-exclusive-edition&amp;controller=product&amp;id_lang=1"
                                           class="product-image blur-image">
                                            <img class="img-responsive product-img1"
                                                 data-src="https://prestashop17.joommasters.com/molla/img/p/5/9/1/591-home_default_200x310.jpg"
                                                 src="https://prestashop17.joommasters.com/molla/img/p/5/9/1/591-home_default_200x310.jpg"
                                                 alt=""
                                                 title="The Durrells of Corfu: Exclusive Edition"
                                                 data-full-size-image-url="https://prestashop17.joommasters.com/molla/img/p/5/9/1/591-large_default.jpg"
                                            />
                                        </a>


                                        <ul class="product-flags">
                                        </ul>

                                    </div>
                                    <div class="product-info">

                                        <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                           onclick="WishlistCart('wishlist_block_list', 'add', '134', false, 1); return false;"
                                           data-id-product="134"></a>
                                        <a class="category-name"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?id_category=61&controller=category&id_lang=1">
                                            by
                                            <span>John Gray</span>
                                        </a>

                                        <h3 class="product-title" itemprop="name"><a class="product-link"
                                                                                     href="https://prestashop17.joommasters.com/molla/index.php?id_product=134&amp;rewrite=the-durrells-of-corfu-exclusive-edition&amp;controller=product&amp;id_lang=1">The
                                            Durrells of Corfu: Exclusive...</a></h3>


                                        <div class="content_price">

						<span class="price new ">
							$24.48
						</span>


                                        </div>

                                        <div class="product-short-desc">
                                            <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae
                                                luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing.
                                                Sed lectus.</p>
                                        </div>
                                        <div class="product-footer">

                                            <div id="review">
                                                <div class="product-list-reviews no-review" data-id="134"
                                                     data-url="https://prestashop17.joommasters.com/molla/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
                                                    <div class="grade-stars small-stars"></div>
                                                    <div class="comments-nb ratings-text"></div>
                                                </div>
                                            </div>


                                            <div class="product-action">
                                                <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                                   title="Add to cart" data-id-product="134" data-minimal-quantity="1"
                                                   data-token="588a5a1eb02312add78ccb857e952842">
                                                    <span>Add to cart</span>
                                                </a>
                                            </div>

                                            <a href="#" class="addToWishlist btn-icon" title="Add to Wishlist"
                                               onclick="WishlistCart('wishlist_block_list', 'add', '134', false, 1); return false;"
                                               data-id-product="134">
                                                <span>Add to Wishlist</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="product-actions">

                                        <div class="content_price">

                                            <span class="price new ">$24.48</span>


                                        </div>

                                        <a href="#" data-link-action="quickview" title="Quick View"
                                           class="btn-icon quick-view">
                                            <span>Quick View</span>
                                        </a>
                                        <a href="#" class="ajax-add-to-cart product-btn  cart-button btn-icon"
                                           title="Add to cart" data-id-product="134" data-minimal-quantity="1"
                                           data-token="588a5a1eb02312add78ccb857e952842">
                                            <span>Add to cart</span>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="row-x91ezwue6" class="gdz-row ">
    <div class="container">
        <div class="row">
            <div id="column-kvq8yps29" class="layout-column col-lg-12 col-md-12 col-12 ">
                <div id="addon-lslgvhxfs" class="addon-box">
                    <h5 class="pb-heading title text-lg-left">
                        From Our Blog
                    </h5>
                </div>
            </div>
            <div id="column-euwx3z980" class="layout-column col-lg-12 col-md-12 col-12 ">
                <div id="addon-phs3konnj" class="addon-box">
                    <div class="pb-blog">
                        <div class="blog-carousel owl-carousel carousel-with-shadow" data-items="4" data-lg="4"
                             data-md="4" data-sm="3" data-xs="1" data-nav="false" data-dots="false" data-auto="false"
                             data-rewind="false" data-slidebypage="1" data-margin="20">
                            <div class="item">
                                <div class="blog-item">
                                    <div class="post-thumb">
                                        <a href="/home.jsp/prestashop17.joommasters.com/molla/index.php?fc=module&amp;module=gdz_blog&amp;category_slug=books&amp;post_id=20&amp;controller=post&amp;id_lang=1">
                                            <img src="https://prestashop17.joommasters.com/molla/modules/gdz_blog/views/img/thumb_fec757e1f27303999d076da80097ff0c.jpg"
                                                 alt="Aenean dignissim pellentesque felis." class="img-responsive"/>
                                        </a>
                                    </div>
                                    <div class="entry-body">
                                        <ul class="post-meta">
                                            <li class="post-created">
                                                Oct 21, 2020
                                            </li>
                                            <li class="post-comments">0 Comment</li>
                                        </ul>
                                        <a class="post-title"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?fc=module&amp;module=gdz_blog&amp;category_slug=books&amp;post_id=20&amp;controller=post&amp;id_lang=1">Aenean
                                            dignissim pellentesque felis.</a>
                                        <div class="post-intro"><p>Morbi in sem quis dui placerat ornare. Pellentesque
                                            odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu.</p></div>
                                        <a class="post-readmore"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?fc=module&amp;module=gdz_blog&amp;category_slug=books&amp;post_id=20&amp;controller=post&amp;id_lang=1">
                                            <span>Continue Read ...</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="blog-item">
                                    <div class="post-thumb">
                                        <a href="/home.jsp/prestashop17.joommasters.com/molla/index.php?fc=module&amp;module=gdz_blog&amp;category_slug=books&amp;post_id=21&amp;controller=post&amp;id_lang=1">
                                            <img src="https://prestashop17.joommasters.com/molla/modules/gdz_blog/views/img/thumb_961394a467ffdfba809898b65658f1c0.jpg"
                                                 alt="Donec nec justo eget felis facilisis fermentum."
                                                 class="img-responsive"/>
                                        </a>
                                    </div>
                                    <div class="entry-body">
                                        <ul class="post-meta">
                                            <li class="post-created">
                                                Oct 21, 2020
                                            </li>
                                            <li class="post-comments">0 Comment</li>
                                        </ul>
                                        <a class="post-title"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?fc=module&amp;module=gdz_blog&amp;category_slug=books&amp;post_id=21&amp;controller=post&amp;id_lang=1">Donec
                                            nec justo eget felis facilisis fermentum.</a>
                                        <div class="post-intro"><p>Donec odio. Quisque volutpat mattis eros. Nullam
                                            malesuada erat ut Suspendisse urna nibh, viverra non, semper suscipit.</p>
                                        </div>
                                        <a class="post-readmore"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?fc=module&amp;module=gdz_blog&amp;category_slug=books&amp;post_id=21&amp;controller=post&amp;id_lang=1">
                                            <span>Continue Read ...</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="blog-item">
                                    <div class="post-thumb">
                                        <a href="/home.jsp/prestashop17.joommasters.com/molla/index.php?fc=module&amp;module=gdz_blog&amp;category_slug=books&amp;post_id=22&amp;controller=post&amp;id_lang=1">
                                            <img src="https://prestashop17.joommasters.com/molla/modules/gdz_blog/views/img/thumb_8bcce59b927f7d26bb935fce60265760.jpg"
                                                 alt="Aliquam porttitor mauris sit." class="img-responsive"/>
                                        </a>
                                    </div>
                                    <div class="entry-body">
                                        <ul class="post-meta">
                                            <li class="post-created">
                                                Oct 21, 2020
                                            </li>
                                            <li class="post-comments">0 Comment</li>
                                        </ul>
                                        <a class="post-title"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?fc=module&amp;module=gdz_blog&amp;category_slug=books&amp;post_id=22&amp;controller=post&amp;id_lang=1">Aliquam
                                            porttitor mauris sit.</a>
                                        <div class="post-intro"><p>Suspendisse urna nibh, viverra semper suscipit.
                                            posuere a, pede. Morbi in sem quis dui placerat ornare.</p></div>
                                        <a class="post-readmore"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?fc=module&amp;module=gdz_blog&amp;category_slug=books&amp;post_id=22&amp;controller=post&amp;id_lang=1">
                                            <span>Continue Read ...</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="blog-item">
                                    <div class="post-thumb">
                                        <a href="/home.jsp/prestashop17.joommasters.com/molla/index.php?fc=module&amp;module=gdz_blog&amp;category_slug=books&amp;post_id=23&amp;controller=post&amp;id_lang=1">
                                            <img src="https://prestashop17.joommasters.com/molla/modules/gdz_blog/views/img/thumb_11f329d43ca9496ce2ce7503c43a001d.jpg"
                                                 alt="Integer vitae libero ac risus egestas placerat."
                                                 class="img-responsive"/>
                                        </a>
                                    </div>
                                    <div class="entry-body">
                                        <ul class="post-meta">
                                            <li class="post-created">
                                                Oct 21, 2020
                                            </li>
                                            <li class="post-comments">0 Comment</li>
                                        </ul>
                                        <a class="post-title"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?fc=module&amp;module=gdz_blog&amp;category_slug=books&amp;post_id=23&amp;controller=post&amp;id_lang=1">Integer
                                            vitae libero ac risus egestas placerat.</a>
                                        <div class="post-intro"><p>Lorem ipsum dolor sit amet, consecte tuer adipiscing
                                            elit. Donec odio. Quisque volutpat mattis eros.</p></div>
                                        <a class="post-readmore"
                                           href="/home.jsp/prestashop17.joommasters.com/molla/index.php?fc=module&amp;module=gdz_blog&amp;category_slug=books&amp;post_id=23&amp;controller=post&amp;id_lang=1">
                                            <span>Continue Read ...</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="row-t1roqf33w" class="gdz-row h20-newsletter">
    <div class="container">
        <div class="row">
            <div id="column-hgqhnftfg" class="layout-column col-lg-12 col-md-12 col-12 ">
                <div id="addon-th0cj01jl" class="addon-box">
                    <div class="gdz-icon-box">
                        <i class="icon-envelope"></i>
                    </div>
                </div>
            </div>
            <div id="column-5xgr5me01" class="layout-column col-lg-12 col-md-12 col-12 ">
                <div id="addon-srnbead6v" class="addon-box">
                    <h5 class="pb-heading title">
                        Subscribe for Our Newsletter
                    </h5>
                </div>
            </div>
            <div id="column-rv50wu6id" class="layout-column col-lg-12 col-md-12 col-12 ">
                <div id="addon-dnvw91jnk" class="addon-box">
                    <div class="pb-text-content"><p>Learn about new offers and get more deals by joining our
                        newsletter</p></div>
                </div>
            </div>
            <div id="column-yfhwli0z7" class="layout-column col-lg-12 col-md-12 col-12 ">
                <div id="addon-uv4dskssh" class="addon-box">
                    <div class="pb-module">
                        <div class="email_subscription block block_newsletter">
                            <h2 class="widget-title">Sign up for email & get 25% off</h2>
                            <p class="widget-desc">Subcribe to get information about products and coupons</p>
                            <div class="block-content">
                                <form action="https://prestashop17.joommasters.com/molla/index.php" method="post">
                                    <div class="input-group newsletter-input-group">
                                        <input type="text" name="email" value="" required class="form-control"
                                               placeholder="Your Email Address"/>
                                        <button type="submit" class="btn btn-popup newsletter-button align-items-center"
                                                name="submitNewsletter">
                                            Go
                                        </button>
                                        <button class="btn btn-primary btn-submit" type="submit" id="newsletter-btn"
                                                name="submitNewsletter">
                                            <span>Subscribe</span>
                                            <i class="icon-long-arrow-right"></i>
                                        </button>
                                    </div>

                                    <input type="hidden" name="action" value="0"/>
                                </form>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/master/foot.jsp" %>
