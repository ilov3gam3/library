package Controller;

import Database.DB;
import Database.MyObject;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

public class BookController {
    @WebServlet("/admin/book")
    @MultipartConfig(
            fileSizeThreshold = 1024 * 1024, // 1 MB
            maxFileSize = 1024 * 1024 * 10,      // 10 MB
            maxRequestSize = 1024 * 1024 * 10  // 10 MB
    )
    public static class BookIndex extends HttpServlet{
        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            ArrayList<MyObject> genres = DB.getData("select * from genre", new String[]{"id", "name", "description"});
            ArrayList<MyObject> authors = DB.getData("select * from authors", new String[]{"id", "name", "dob", "nationality", "biography", "image"});
            String sql = "select books.*, authors.name as author_name, genre.name as genre_name from books inner join authors on books.author_id = authors.id inner join genre on books.genre_id = genre.id";
            ArrayList<MyObject> books = DB.getData(sql, new String[]{"id", "title", "description", "author_id", "genre_id", "quantity", "cover_image","price", "soft_file", "available", "author_name", "genre_name"});
            req.setAttribute("books", books);
            req.setAttribute("genres", genres);
            req.setAttribute("authors", authors);
            req.getRequestDispatcher("/views/admin/book.jsp").forward(req, resp);
        }

        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            String title = req.getParameter("title");
            String description = req.getParameter("description");
            String author_id = req.getParameter("author_id");
            String genre_ids = req.getParameter("genre_id");
            String quantity = req.getParameter("quantity");
            String price = req.getParameter("price");
            Part filePart = req.getPart("image");
            String fileName = HandleFileUpload.getFileName(filePart);
            assert fileName != null;
            String newFileName = HandleFileUpload.generateUniqueFileName(fileName);
            String uploadDir = req.getServletContext().getRealPath("/") + "uploads";
            Path filePath = Paths.get(uploadDir, newFileName);
            try (InputStream fileContent = filePart.getInputStream()) {
                Files.copy(fileContent, filePath, StandardCopyOption.REPLACE_EXISTING);
            }
            String cover_image = "/uploads/" + newFileName;

            filePart = req.getPart("soft_file");
            fileName = HandleFileUpload.getFileName(filePart);
            assert fileName != null;
            newFileName = HandleFileUpload.generateUniqueFileName(fileName);
            uploadDir = req.getServletContext().getRealPath("/") + "uploads";
            filePath = Paths.get(uploadDir, newFileName);
            try (InputStream fileContent = filePart.getInputStream()) {
                Files.copy(fileContent, filePath, StandardCopyOption.REPLACE_EXISTING);
            }

            String soft_file = "/uploads/" + newFileName;

            String sql = "insert into books(title, description, author_id, genre_id, quantity, cover_image, soft_file, available,price) values (?,?,?,?,?,?,?,?,?)";
            String[] vars = new String[]{title, description, author_id, genre_ids, quantity, cover_image, soft_file, "true",price};
            boolean check = DB.executeUpdate(sql, vars);
            if (check){
                req.setAttribute("mess", "success|Thêm sách thành công");
            } else {
                req.setAttribute("mess", "error|Thêm sách không thành công");
            }
            resp.sendRedirect(req.getContextPath() + "/admin/book");
        }
    }
}
