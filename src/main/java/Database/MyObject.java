package Database;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class MyObject {
    public String id;
    public String name;
    public String email;
    public String phone;
    public String avatar;
    public String dob;
    public String address;
    public String password;
    public String is_verify;
    public String role_id;
    public String is_block;
    public String uuid;
    public String gender;
    public String re_password;
    public String role_name;
    public String description;
    public String nationality;
    public String biography;
    public String image;
    public String title;
    public String author_id;
    public String author_name;
    public String genre_id;
    public String genre_name;
    public String quantity;
    public String cover_image;
    public String soft_file;
    public String available;
    public String price;


    public static void main(String[] args) {
    }
}